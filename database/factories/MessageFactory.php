<?php

namespace Database\Factories;

use App\Models\Message;
use Illuminate\Database\Eloquent\Factories\Factory;
//use Illuminate\Support\Str;
use Faker\Provider\fr_FR\PhoneNumber;

class MessageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Message::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'from' => $this->faker->phoneNumber(),
            'to' => $this->faker->phoneNumber(),
            'message_id' => $this->faker->regexify('[A-Z0-9]{17}'),
            // 'message_timestamp' => '2021-01-01 12:13:00',
            'message_timestamp' => $this->faker->dateTimeBetween('2021-01-01 12:13:00', '2021-02-01 12:13:00'),
            'message' => $this->faker->paragraph(1),
        ];
    }
}
