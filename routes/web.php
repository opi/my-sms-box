<?php

use Illuminate\Support\Facades\Route;

use App\Models\Message;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/messages');

Route::get('/messages', function() {
    $messages = Message::all()->sortByDesc('message_timestamp');
    return view('message.list', ['messages' => $messages]);
})->name('inbox')->middleware(['auth']);

Route::get('/messages/{message}', function(Message $message) {
    return view('message.view', ['message' => $message]);
})->name('message')->middleware(['auth']);

Route::get('/messages/{message}/delete', function(Message $message) {
    return view('message.delete', ['message' => $message]);
})->name('message.delete')->middleware(['auth']);

Route::post('/messages/{message}/delete', function(Message $message) {
    // Delete message
    //Message::where('id', $message->id)->delete();
    Message::find($message->id)->delete();
    // Status message
    $status = "Message successfully deleted.";
    
    return redirect()->route('inbox')->with('status', $status);
})->middleware(['auth']);

Route::get('/messages/{message}/reply', function(Message $message) {
    return view('message.reply', ['message' => $message]);
})->name('message.reply')->middleware(['auth']);


Route::get('/send', function() {
    return view('message.send');
})->name('message.send')->middleware(['auth']);

Route::post('/send', function(Request $request) {
    $to = $request->input('to');
    $text = $request->input('message');
    // Max 160 char

    $vonage_api_key = env('VONAGE_API_KEY');
    $vonage_api_secret = env('VONAGE_API_SECRET');
    $basic  = new \Vonage\Client\Credentials\Basic($vonage_api_key, $vonage_api_secret);
    $client = new \Vonage\Client($basic);

    $brand_name = env('VONAGE_BRAND_NAME');
    $response = $client->sms()->send(
        new \Vonage\SMS\Message\SMS($to, $brand_name, $text)
    );
    $message = $response->current();
    
    if ($message->getStatus() == 0) {
        $status = "The message was sent successfully to $to";
    } else {
        $status = "The message failed with status: " . $message->getStatus();
    }

    return redirect()->route('inbox')->with('status', $status);
})->name('message.send')->middleware(['auth']);


Route::get('/dashboard', function (User $user) {
    return view('dashboard', ['user' => $user]);
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
