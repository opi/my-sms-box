<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Models\Message;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



Route::get('/receive', function(Request $request) {
    Log::debug("GET /receive");
    
    // $sms = \Vonage\SMS\Webhook\Factory::createFromRequest($request);
    // Log::debug('From: ' . $sms->getMsisdn() . ' message: ' . $sms->getText());
    
    // $payload = json_decode($request->getContent(), true);
    $payload = $request->all();
    Log::debug(print_r($payload, 1));
// print_r($request->all());

/**
[2021-02-07 08:23:46] local.DEBUG: Array
(
    [msisdn] => 33601757160
    [to] => 33644636267
    [messageId] => 16000002E4EB39DF
    [text] => Envoyé à 9h23
    [type] => text / unicode
    [keyword] => ENVOYÉ
    [api-key] => 2e1c700a
    [message-timestamp] => 2021-02-07 08:23:45
)
 */

    // Save message
    $message = new Message;
    $message->from = $payload['msisdn'] ?? 'FROM';
    $message->to = $payload['to'] ?? 'TO';
    $message->message = $payload['text'] ?? 'TEXT';
    $message->message_id = $payload['messageId'] ?? 'MESSAGE ID';
    $message->message_timestamp = $payload['message-timestamp'] ?? NULL;
    $message->save();
    
    // Return 200
    return "OK";
})->name('api_receive')->middleware('guest');
