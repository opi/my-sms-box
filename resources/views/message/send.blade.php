@extends('layout')

@section('title', __('Send a message'))

@section('content')

<form method="POST" action="{{ route('message.send') }}">
    @csrf

    <div class="field">
        <label class="label" for="to">{{ __('Message recipient') }}</label>
        <div class="control">
            <input class="input" type="text" id="to" name="to" value="" placeholder="A valide phone number" required>
        </div>
        <p class="help">{{ __('Use international number without the leading +. Example: 33601020304')}}</p>
    </div>

    <div class="field">
        <label class="label" for="message">{{ __('Message body') }}</label>
        <div class="control">
            <textarea class="textarea" name="message" id="message" placeholder="{{ __('Your message ...') }}" required></textarea>
        </div>
    </div>

    <div class="field is-grouped">
        <div class="control">
            <input class="button is-primary" type="submit" value="{{ __('Send') }}" />
        </div>
    </div>

</form>

@endsection