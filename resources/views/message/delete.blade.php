@extends('layout')

@section('title', __('Delete a message'))

@section('content')

<form method="POST" action="{{ route('message.delete', ['message' => $message->id]) }}">
    @csrf

    <div class="message-item box">
        <header class="is-medium columns">
            <p class="column">Date: {{ $message->created_at }}</p>
            <p class="column">From: {{ $message->from }}</p>
            <p class="column">To: {{ $message->to }}</p>
        </header>
        <p class="message-content content">{{ $message->message }}</p>
    </div>
    
    <p class="title is-4">{{ __('Are you sure ?') }}</p>

    <div class="field is-grouped is-grouped-multiline">
        <div class="control">
            <input class="button is-danger" type="submit" value="{{ __('Confirm deletion') }}" />
        </div>
        <div class="control">
            <a class="button is-info is-light" href="{{ route('message', ['message' => $message->id]) }}">{{ __('Back to the message')}}</a>
        </div>
    </div>

</form>

<!-- <div class="block inbox-back-link">
    <a class="button is-light" href="{{ route('inbox') }}">ᐸ Inbox</a>
</div> -->

@endsection