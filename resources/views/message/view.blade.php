@extends('layout')

@section('title', __('View message detail'))

@section('content')

<div class="message-item box">
    <header class="is-medium columns">
        <p class="column">{{ __('Date: :date', ['date' => $message->message_timestamp])}}</p>
        <p class="column">{{ __('From: :from', ['from' => $message->from]) }}</p>
        <p class="column">{{ __('To: :to', ['to' => $message->to]) }}</p>
    </header>
    <p class="message-content content">{!! nl2br(e($message->message)) !!}</p>
    <div class="buttons">
        <a class="button is-primary" href="{{ route('message.reply', ['message' => $message->id]) }}">{{ __('Reply') }}</a>
        <a class="button is-danger" href="{{ route('message.delete', ['message' => $message->id]) }}">{{ __('Delete') }}</a>
    </div>
</div>

<!-- <div class="block inbox-back-link">
    <a class="button is-light" href="{{ route('inbox') }}">ᐸ Inbox</a>
</div> -->

@endsection