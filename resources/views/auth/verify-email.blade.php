@extends('layout')

@section('title', __('Reset Password'))

@section('content')

    <div class="">
        {{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}
    </div>

    @if (session('status') == 'verification-link-sent')
        <div class="">
            {{ __('A new verification link has been sent to the email address you provided during registration.') }}
        </div>
    @endif

    <form method="POST" action="{{ route('verification.send') }}">
        @csrf
        <div class="field is-grouped">
            <div class="control">
                <input class="button is-primary" type="submit" value="{{ __('Resend Verification Email') }}" />
            </div>
        </div>
    </form>

    <form method="POST" action="{{ route('logout') }}">
            @csrf
            <div class="field is-grouped">
                <div class="control">
                    <input class="button is-primary" type="submit" value="{{ __('Logout') }}" />
                </div>
            </div>
    </form>

@endsection