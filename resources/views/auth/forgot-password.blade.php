@extends('layout')

@section('title', __('Forgot Password'))

@section('content')

    <div class="">
        {{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
    </div>


    <form method="POST" action="{{ route('password.email') }}">
        @csrf

        <div class="field">
            <label class="label" for="email">{{ __('Email') }}</label>
            <div class="control">
                <input id="email" class="input" type="email" name="email" :value="old('email')" required autofocus />
            </div>
        </div>
        
        <div class="field is-grouped">
            <div class="control">
                <input class="button is-primary" type="submit" value="{{ __('Email Password Reset Link') }}" />
            </div>
        </div>
        
        <div class="field">
            @if (Route::has('login'))
            <a class="" href="{{ route('login') }}">
                {{ __('Login?') }}
            </a>
            @endif
        </div>
    </form>

@endsection