@extends('layout')

@section('title', __('Register'))

@section('content')

    <form method="POST" action="{{ route('register') }}">
        @csrf

        <div class="field">
            <label class="label" for="name">{{ __('Name') }}</label>
            <div class="control">
                <input id="name" class="input" type="text" name="name" :value="old('name')" required autofocus />
            </div>
        </div>

        <div class="field">
            <label class="label" for="email">{{ __('Email') }}</label>
            <div class="control">
                <input id="email" class="input" type="email" name="email" :value="old('email')" required />
            </div>
        </div>

        <div class="field">
            <label class="label" for="password">{{ __('Password') }}</label>
            <div class="control">
                <input id="password" class="input" type="password" name="password" :value="old('password')" required autocomplete="new-password"  />
            </div>
        </div>
        <div class="field">
            <label class="label" for="password_confirmation">{{ __('Confirm Password') }}</label>
            <div class="control">
                <input id="password_confirmation" class="input" type="password" name="password_confirmation" :value="old('password')" required />
            </div>
        </div>
        
        <div class="field is-grouped">
            <div class="control">
                <input class="button is-primary" type="submit" value="{{ __('Register') }}" />
            </div>
        </div>

        <div class="field">
            <a class="" href="{{ route('login') }}">
                {{ __('Already registered?') }}
            </a>
        </div>
    </form>

@endsection