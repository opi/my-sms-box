@extends('layout')

@section('title', __('Reset Password'))

@section('content')


    <form method="POST" action="{{ route('password.update') }}">
        @csrf

        <!-- Password Reset Token -->
        <input type="hidden" name="token" value="{{ $request->route('token') }}">

        <div class="field">
            <label class="label" for="email">{{ __('Email') }}</label>
            <div class="control">
                <input id="email" class="input" type="email" name="email" :value="old('email', $request->email)" required autofocus />
            </div>
        </div>

        <div class="field">
            <label class="label" for="password">{{ __('Password') }}</label>
            <div class="control">
                <input id="password" class="input" type="password" name="password" :value="old('password')" required />
            </div>
        </div>
        <div class="field">
            <label class="label" for="password_confirmation">{{ __('Confirm Password') }}</label>
            <div class="control">
                <input id="password_confirmation" class="input" type="password" name="password_confirmation" :value="old('password')" required />
            </div>
        </div>
        
        <div class="field is-grouped">
            <div class="control">
                <input class="button is-primary" type="submit" value="{{ __('Reset Password') }}" />
            </div>
        </div>
    </form>
    
@endsection