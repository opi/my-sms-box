@extends('layout')

@section('title', __('Confirm Password'))

@section('content')

    <div class="">
        {{ __('This is a secure area of the application. Please confirm your password before continuing.') }}
    </div>

    <form method="POST" action="{{ route('password.confirm') }}">
        @csrf

        <div class="field">
            <label class="label" for="password">{{ __('Password') }}</label>
            <div class="control">
                <input id="password" class="input" type="password" name="password" :value="old('password')" required autocomplete="current-password"  />
            </div>
        </div>

        <div class="field is-grouped">
            <div class="control">
                <input class="button is-primary" type="submit" value="{{ __('Confirm') }}" />
            </div>
        </div>
    </form>

@endsection