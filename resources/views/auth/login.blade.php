@extends('layout')

@section('title', __('Login'))

@section('content')

    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="field">
            <label class="label" for="email">{{ __('Email') }}</label>
            <div class="control">
                <input id="email" class="input" type="email" name="email" :value="old('email')" required autofocus />
            </div>
        </div>

        <div class="field">
            <label class="label" for="email">{{ __('Password') }}</label>
            <div class="control">
                <input id="password" class="input" type="password" name="password" required autocomplete="current-password" />
            </div>
        </div>

        <div class="field">
            <label class="checkbox" for="remember_me">
            <input id="remember_me" type="checkbox" name="remember">
            {{ __('Remember me') }}
            </label>
        </div>

        <div class="field is-grouped">
            <div class="control">
                <input class="button is-primary" type="submit" value="{{ __('Login') }}" />
            </div>
        </div>

        <div class="field">
        @if (Route::has('password.request'))
            <a class="" href="{{ route('password.request') }}">
                {{ __('Forgot your password?') }}
            </a>
        @endif
        <div class="field">
        @if (Route::has('register'))
            <a class="" href="{{ route('register') }}">
                {{ __('Register?') }}
            </a>
        @endif
        </div>

    </form>

@endsection