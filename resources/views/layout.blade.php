<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>{{ config('app.name') }} - @yield('title')</title>
        <link rel="stylesheet" href="/css/app.css" />
    </head>
    <body>

        <header>
            <div class="container">
                <h1 class="title"><a href="/">{{ config('app.name') }}</a></h1>
                <h1 class="subtitle">@yield('title')</h1>
                
            </div>
        </header>
        <div class="container">
                <nav class="navbar" role="navigation" aria-label="main navigation">
                    <!-- <div class="navbar-brand">
                        <a class="navbar-item" href="/">My SMS Inbox</a>
        
                        <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                        </a>
                    </div> -->
        
                    <div id="navbarBasicExample" class="navbar-menu">
                        <div class="navbar-start">
                        @auth
                            <a href="{{ route('inbox') }}" class="navbar-item @if (Route::is('inbox')) is-active @endif">{{ __('Inbox') }}</a>
                            <a href="{{ route('message.send') }}" class="navbar-item @if (Route::is('message.send')) is-active @endif ">{{ __('Send') }}</a>
                        @endauth
                        @guest
                        <a href="{{ route('login') }}" class="navbar-item">{{ __('Login') }}</a>
                        @endguest
                        </div>

                        <div class="navbar-end">
                        @auth
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <input  class="button is-ghost navbar-item" type="submit" value="{{ __('Logout') }}" />
                        </form>
                        @endauth
                        </div>
                    </div>
                </nav>

        </div>
        



        <main>
            <div class="container">
                @if (session('status'))
                <div class="notification is-primary">
                    {{ session('status') }}
                </div>
                @endif

                @if ($errors->any())
                @foreach ($errors->all() as $error)
                <div class="notification is-danger">{{ $error }}</div>
                @endforeach
                @endif

                <div class="main-content">
                @yield('content')
                </div>
            </div>
        </main>
    </body>
</html>